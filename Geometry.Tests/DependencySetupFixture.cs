using Geometry.Core;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Geometry.Tests
{
    public class DependencySetupFixture : IDisposable
    {
        public ServiceProvider ServiceProvider { get; }

        public DependencySetupFixture()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddSingleton<IFigureFactory, FigureFactory>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        public void Dispose()
        {
            ServiceProvider.Dispose();
        }
    }
}