using Geometry.Core;
using Geometry.Core.Models;
using Geometry.Db;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Geometry.Tests
{
    public class FigureFactoryTests : IClassFixture<DependencySetupFixture>
    {
        private readonly ServiceProvider _serviceProvider;

        public FigureFactoryTests(DependencySetupFixture fixture)
        {
            _serviceProvider = fixture.ServiceProvider;
        }

        [Fact]
        public void FigureFactory_CanDeserializeCircle()
        {
            using var scope = _serviceProvider.CreateScope();
            var factory = scope.ServiceProvider.GetRequiredService<IFigureFactory>();

            // Arrange
            var targetFigure = new Circle()
            {
                Radius = 155
            };
            var json = targetFigure.GetJson();

            // Act
            var figure = factory.CreateFigure(FigureType.Circle, json) as Circle;

            // Assert
            Assert.NotNull(figure);
            Assert.Equal(figure.Radius, targetFigure.Radius);
        }

        [Fact]
        public void FigureFactory_CanDeserializeTriangle()
        {
            using var scope = _serviceProvider.CreateScope();
            var factory = scope.ServiceProvider.GetRequiredService<IFigureFactory>();

            // Arrange
            var targetFigure = new Triangle()
            {
                Side1 = 10,
                Side2 = 5,
                Angle = 45
            };
            var json = targetFigure.GetJson();

            // Act
            var figure = factory.CreateFigure(FigureType.Triangle, json) as Triangle;

            // Assert
            Assert.NotNull(figure);
            Assert.Equal(figure.Side1, targetFigure.Side1);
            Assert.Equal(figure.Side2, targetFigure.Side2);
            Assert.Equal(figure.Angle, targetFigure.Angle);
        }

        [Fact]
        public void FigureFactory_CanDeserializeRectangle()
        {
            using var scope = _serviceProvider.CreateScope();
            var factory = scope.ServiceProvider.GetRequiredService<IFigureFactory>();

            // Arrange
            var targetFigure = new Rectangle()
            {
                Side1 = 10,
                Side2 = 5,
            };
            var json = targetFigure.GetJson();

            // Act
            var figure = factory.CreateFigure(FigureType.Triangle, json) as Triangle;

            // Assert
            Assert.NotNull(figure);
            Assert.Equal(figure.Side1, targetFigure.Side1);
            Assert.Equal(figure.Side2, targetFigure.Side2);
        }
    }
}
