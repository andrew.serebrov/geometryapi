﻿using Geometry.Api.Models;
using Geometry.Core;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Geometry.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FigureController : ControllerBase
    {
        private readonly IFigureFactory _figureFactory;
        private readonly IFigureService _figureService;

        public FigureController(IFigureFactory figureFactory, IFigureService figureService)
        {
            _figureFactory = figureFactory;
            _figureService = figureService;
        }

        /// <summary>
        /// Adds a figure into the storage.
        /// </summary>
        /// <param name="modelType">Figure metadata.</param>
        /// <returns>Identifier of a newly added figure.</returns>
        [HttpPost]
        public async Task<IActionResult> Index([FromBody] FigureModelType modelType)
        {
            var figure = _figureFactory.CreateFigure(modelType.Type, modelType.Figure.ToString());
            var id = await _figureService.AddFigureAsync(figure, modelType.Type);

            return Ok(id);
        }

        /// <summary>
        /// Calculates an area of a figure by the given identifier.
        /// </summary>
        /// <param name="id">Identifier of the figure.</param>
        /// <returns>Area of a figure.</returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetSquare(long id)
        {
            var figure = await _figureService.GetFigureByIdAsync(id);
            if (figure == null)
                return NotFound(id);

            var area = figure.CalcArea();
            return Ok(area);
        }
    }
}
