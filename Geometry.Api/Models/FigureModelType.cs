﻿using Geometry.Db;

namespace Geometry.Api.Models
{
    public class FigureModelType
    {
        public FigureType Type { get; set; }

        public object Figure { get; set; }
    }
}