﻿using Geometry.Core.Models;
using Geometry.Db;

namespace Geometry.Core
{
    public interface IFigureFactory
    {
        FigureBase CreateFigure(FigureType type, string json);
    }
}