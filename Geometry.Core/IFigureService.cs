﻿using Geometry.Core.Models;
using Geometry.Db;
using System.Threading.Tasks;

namespace Geometry.Core
{
    public interface IFigureService
    {
        Task<long> AddFigureAsync(FigureBase figure, FigureType figureType);

        Task<FigureBase> GetFigureByIdAsync(long id);
    }
}