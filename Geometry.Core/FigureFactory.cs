﻿using Geometry.Core.Models;
using Geometry.Db;
using System;
using System.Text.Json;

namespace Geometry.Core
{
    public class FigureFactory: IFigureFactory
    {
        public FigureBase CreateFigure(FigureType type, string json)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            switch (type)
            {
                case FigureType.Circle:
                    return JsonSerializer.Deserialize<Circle>(json, options);
                case FigureType.Triangle:
                    return JsonSerializer.Deserialize<Triangle>(json, options);
                case FigureType.Rectangle:
                    return JsonSerializer.Deserialize<Rectangle>(json, options);
                default:
                    throw new NotImplementedException(nameof(type));
            }
        }
    }
}