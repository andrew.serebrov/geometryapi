﻿using Geometry.Db;
using Geometry.Db.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Geometry.Core
{
    public static class DbServiceCollectionExtensions
    {
        public static IServiceCollection AddDb(this IServiceCollection services, string connectionString)
        {
            var assemblyName = "Geometry.Db";
            services.AddDbContextFactory<FigureDbContext>(options =>
                options.UseNpgsql(connectionString, b => b.MigrationsAssembly(assemblyName)));

            services.AddTransient<IFigureService, FigureService>();

            return services;
        }
    }
}