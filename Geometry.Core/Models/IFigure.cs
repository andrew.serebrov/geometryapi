﻿namespace Geometry.Core.Models
{
    public interface IFigure
    {
        public double CalcArea();
    }
}
