﻿using Geometry.Db;
using System.Text.Json;

namespace Geometry.Core.Models
{
    public abstract class FigureBase : IFigure
    {
        public FigureType Type { get; set; }
        public abstract double CalcArea();

        public string GetJson()
        {
            var options = new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true
            };
            return JsonSerializer.Serialize(this, this.GetType(), options);
        }
    }
}