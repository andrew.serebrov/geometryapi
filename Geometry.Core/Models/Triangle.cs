﻿using System;

namespace Geometry.Core.Models
{
    public class Triangle : FigureBase
    {
        /// <summary>
        /// Angle in degrees
        /// </summary>
        public double Angle { get; set; }
        public double Side1 { get; set; }
        public double Side2 { get; set; }

        public override double CalcArea()
        {
            return Side1 * Side2 * 0.5 * Math.Sin(Angle * Math.PI / 180);
        }
    }
}