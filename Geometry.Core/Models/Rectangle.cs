﻿namespace Geometry.Core.Models
{
    public class Rectangle : FigureBase
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }

        public override double CalcArea()
        {
            return Side2 * Side1;
        }
    }
}