﻿using System;

namespace Geometry.Core.Models
{
    public class Circle : FigureBase
    {
        public double Radius { get; set; }

        public override double CalcArea()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}