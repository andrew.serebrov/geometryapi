﻿using Geometry.Core.Models;
using Geometry.Db.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Geometry.Db;

namespace Geometry.Core
{
    public class FigureService: IFigureService
    {
        private readonly IFigureFactory _figureFactory;
        private readonly IDbContextFactory<FigureDbContext> _contextFactory;

        public FigureService(IDbContextFactory<FigureDbContext> contextFactory, IFigureFactory figureFactory)
        {
            _contextFactory = contextFactory;
            _figureFactory = figureFactory;
        }

        public async Task<long> AddFigureAsync(FigureBase figure, FigureType figureType)
        {
            await using var context = _contextFactory.CreateDbContext();
            var entity = new Figure
            {
                Type = figureType,
                Json = figure.GetJson()
            };
            await context.Figures.AddAsync(entity);
            await context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<FigureBase> GetFigureByIdAsync(long id)
        {
            await using var context = _contextFactory.CreateDbContext();

            var entity = await context.Figures.FirstOrDefaultAsync(f => f.Id == id);
            if (entity == null)
                return null;

            return _figureFactory.CreateFigure(entity.Type, entity.Json);
        }
    }
}