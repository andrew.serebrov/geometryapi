﻿namespace Geometry.Db
{
    public enum FigureType
    {
        Circle = 0,
        Triangle = 1,
        Rectangle = 2
    }
}
