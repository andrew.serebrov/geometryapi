﻿using Geometry.Db.Entities;
using Microsoft.EntityFrameworkCore;

namespace Geometry.Db
{
    public class FigureDbContext : DbContext
    {
        public FigureDbContext(DbContextOptions<FigureDbContext> options)
            : base(options)
        {
        }

        public DbSet<Figure> Figures { get; set; }

    }
}