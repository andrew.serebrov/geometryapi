﻿namespace Geometry.Db.Entities
{
    public class Figure
    {
        public long Id { get; set; }
        public string Json { get; set; }
        public FigureType Type { get; set; }
    }
}